---
layout: markdown_page
title: "Category Direction - Infrastructure as Code"
description: "GitLab focus on providing tight integration with best of breed IaC tools, so that all infrastructure related workflows in GitLab are well supported. Learn more!"
canonical_path: "/direction/configure/infrastructure_as_code/"
---

- TOC
{:toc}

## Infrastructure as Code

Infrastructure as code (IaC) is the practice of managing and provisioning infrastructure through machine-readable definition files, rather than physical hardware configuration or interactive configuration tools. The IT infrastructure managed by this comprises both physical equipment such as bare-metal servers as well as virtual machines and associated configuration resources. The definitions are stored in a version control system. IaC takes proven coding techniques and extends them to your infrastructure directly, effectively blurring the line between what is an application and what is the environment.

Our focus will be to provide tight integration with best of breed IaC tools, such that all infrastructure related workflows in GitLab are well supported.
Our initial focus will likely be on Terraform.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Category%3AInfrastructure%20as%20Code)
- [Overall Vision](/direction/configure)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)

Infrastructure and Application provisioning shouldn't be mixed! Application delivery is covered by our [Continuous Delivery directions](https://about.gitlab.com/direction/release/continuous_delivery/).
{: .note .font-small}

### Vision

Our vision is to provide several levels of Infrastructure as Code support:

1. **GitLab Managed IaC** means that GitLab runs all your IaC code based on our reference architectures and your choice of provider
2. **Self-managed IaC through GitLab** allows you to build you custom architecture, likely based on our recommendations, but still use the engines supporting IaC provided by GitLab.
3. **Fully self-managed IaC** is always available to you. This means that you maintain your CI/CD files and pipelines totally. This is already available today.

GitLab supports transitioning traditional I&O teams to automated Infrastructure as Code solutions from the first steps of providing a source code repository to enabling developers with self-service solutions.

### Market overview

According to [Gartner](https://drive.google.com/file/d/1DjsnkQxozkOONAFNLlZaJ5QHzaaY3Lcx/view), I&O automation is the second most common investment target in 2021. Adoption is often led by the move to cloud, but it's expected to rise from 5% to 40% by 2023 for on-prem infrastructure provisioning too. The biggest obstacles for adoption are the lack of developers educated in the area.

## Target User

With GitLab's Infrastructure as Code support - in the order of importance - we are  targeting:

- Platform Engineers, who are in charge of providing the infrastructure for production and non-production environments
- Development Team Leaders, who want low-friction tools in their development processes and have to fulfill various reporting needs
- Developers, who want fast iteration and simple, repeatable deployments

### Interaction with Policy as Code

As Infrastructure as Code usage scales across teams [collaboration pain points around security, compliance and adopting best practices arise](https://www.youtube.com/watch?v=Vy8s7AAvU6g&amp=&feature=youtu.be). Traditionally these pain points are solved by written documentation. Modern infrastructure as code applications have implemented Policy as Code tools to enable automated checking of infrastructure definitions against easy to write policy definitions. One prime example of this is [Hashicorp's Sentinel](https://www.hashicorp.com/sentinel/).  

The principles of Policy as Code are closely aligned with Infrastructure as Code. Within GitLab our existing primitives of integrated CI with *CI job definition in-code* model similar behavior to modern Policy as Code frameworks. At present our existing CI approach allows easy integration of special Policy as Code tools and GitLab.

### What's next

We have a dedicated page for [Kubernetes management directions](https://about.gitlab.com/direction/configure/kubernetes_management/). The following directions are related to generic Infrastructure as Code tool support in GitLab.

First, we are going to focus on Terraform support, the *de facto* standard in infrastructure provisioning. Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1925) where
we discuss this topic and can answer any questions you may have. Your contributions
are welcome!

#### GitLab Managed Terraform State

We would like to provide GitLab users with an unmatched Terraform experience. This involves a Terraform backend that integrates with GitLab pipelines without any setup from the user, and allows advanced state management from within GitLab.

The next steps for GitLab Managed Terraform State are tos provide [protected state files support](https://gitlab.com/gitlab-org/gitlab/-/issues/227108), and provide [management interfaces to support debugging infrastructure issues](https://gitlab.com/gitlab-org/gitlab/-/issues/271591). With protected state files we want to support various use cases where developers and project maintainers have different authorization rights around accessing the infrastructure state file. The management interfaces provide basic administration tasks around state files, like locking, unlocking and removal of the state file. Moreover, it provides views for comparing different versions of a state file.

Please contribute to [our plans in the related epic](https://gitlab.com/groups/gitlab-org/-/epics/2673).

#### Collaboration around Infrastructure changes

In the case of Infrastructure as Code, a code change means a change in the infrastructure too. Thus we want to provide a solution that allows our users to have an overview of both the code changes and the final infrastructure changes in Merge Requests.

We've already shipped the first iteration of the Terraform plan in MR widget, and we are working on [further improving the experience](https://gitlab.com/groups/gitlab-org/-/epics/3441).

#### Terraform module registry

For larger infrastructures, re-usable modules are a call part of the IaC codebase. We intend to provide a Terraform module registry as part of GitLab by extending the current module registries. Please, contribute to our plans around the registry in the [Terraform registry Epic](https://gitlab.com/groups/gitlab-org/-/epics/3221)

#### Improving GitLab's Ansible support

While Terraform is the *de facto standard* for infrastructure provisioning, there are other tools for configuration management, like Ansible or Chef. Following the Terraform developments, we intend to turn our attention to Ansible, and improve Ansible support inside GitLab. You are invited to express your interests and describe your use-case in [Ansible integration in our related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/229633).

### Current maturity

As GitLab's Terraform features are used by around 15% of our potential users, we consider our basic feature to have a great product market fit, and consider the category to be `Viable`. We do not have a scope for what a `Complete` solution might be.

### Analyst Landscape

We don't consider GitLab a replacement of IaC tools, but rather a complement. Based on several discussions, we consider
Terraform the *de facto* standard of infrastructure provisioning.

Given the trends around containerization, ephemeral and immutable infrastructures, we expect the relevance of configuration management tools to decrease, and infrastructure provisioning to gain more market share.

### Top Customer Success/Sales issue(s)

As already mentioned, we've several customers using IaC solutions with GitLab. The following list shows our primary
points of contacts for customer interviews around IaC.

- Wag!
- kiwi.com
- Ooma
- GitLab

### Top user issue(s)

Our top user issues were already presented in the "What's next" section.

### Top internal customer issue(s)

- [Use the GitLab Managed Terraform state for gitlab.com](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10179)

### Competitors

#### CloudSkiff

CloudSkiff describes itself as a CI/CD for Terraform, on steroids.

#### HashiCorp Terraform Enterprise

We consider HashiCorp to be a partner, not a competitor, and we do not support many advanced features offered by Terraform Enterprise.

### Examples

- [How GitLab uses Terraform](https://about.gitlab.com/topics/gitops/gitlab-enables-infrastructure-as-code/) internally
- [Kiwi.com on Infrastructure as Code](https://www.youtube.com/watch?v=Un2mJrRFSm4) at GitLab Commit London, 2019
- Presenting [code.siemens.com](https://www.youtube.com/watch?v=4Y8zv1TJRlM) at GitLab Commit London, 2019
