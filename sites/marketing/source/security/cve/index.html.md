---
layout: markdown_page
title: CVE Numbering Authority
description: "CNAs (CVE Numbering Authorities) may issue CVE identifiers to vulnerabilities in projects within the CNA's scope."
canonical_path: "/security/cve/"
---

## CVEs and CNAs

CVEs ([Common Vulnerability Enumeration)](https://cve.mitre.org/index.html) are
unique identifiers assigned to specific vulnerabilities within a product,
having the form `CVE-YYYY-NNNNN`, with `YYYY` being the year and `NNNNN` being
a unique number for that year.

CNAs ([CVE Numbering Authorities](https://cve.mitre.org/cve/cna.html)) may
issue CVE identifiers to vulnerabilities in projects within the
CNA's scope. To obtain a CVE identifier for an identified vulnerability within
a CNA's scope, parties must contact the CNA and request a CVE identifier for the
vulnerability.

## GitLab's Role as a CNA

GitLab [is a participant in MITRE's CNA program](https://cve.mitre.org/cve/request_id.html#g).

GitLab's scope as a CNA is:

> The GitLab application, any project hosted on GitLab.com in a *public*
> repository, and any vulnerabilities discovered by GitLab that are not in
> another CNA’s scope

CVEs that have been assigned and published by GitLab can be found in the
[gitlab-org/cves project](https://gitlab.com/gitlab-org/cves).

## Requesting a CVE from GitLab

Maintainers of public projects hosted on GitLab.com may request CVEs for
vulnerabilities within their project by creating a
[confidential issue on gitlab-org/cves](https://gitlab.com/gitlab-org/cves/-/issues/new?issue[confidential]=true&issuable_template=Manual%20Submission&issue[title]=Vulnerability%20Submission)
for their CVE request.

Non-maintainers must work with the maintainer of the project to request a CVE
for a vulnerability. It is recommended that a confidential issue first be created
on the project itself to report the vulnerability to the maintainer. The
maintainer of the project is responsible for requesting the CVE identifier from
GitLab.

We will acknowledge receipt of CVE requests the next business day
and strive to send regular updates about our progress. Our goal is to
determine if the vulnerability is valid and communicate back to the submitter
within 30 business days.

If the status of the CVE request is unclear, please feel free to ping us
(`@gitlab-org/secure/vulnerability-research`) within the created issue. If
encrypted email is preferred, download our key from
the [MIT PGP key server](https://pgp.mit.edu/pks/lookup?op=get&search=0xFA6DEFCB219262C8)
or find it [below](#cve-public-gpg-key), and email us at `cve@gitlab.com`.

## CVE Public GPG Key

 * `GitLab CVE <cve@gitlab.com>`
 * ID: FA6DEFCB219262C8
 * Fingerprint 8DEEE034553BF98F760CA9D2FA6DEFCB219262C8

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBF653qgBEACuTDL3NDNhJpXrZAb1eS1Lf29zGx5dXYSikjNEZiTlLJ12AYPC
x0bBxQnRrUuGvjWp1YKjKfD41cNXo5T3bjRmp3hFHJp9jahrbHmYP7etsoyxB+vu
h3h/ZimqqKzZpK0wMv8CCAEOgwnWOEaeI5ktiKrfV4BM3GcTiJ2a2rPwV/IkUc6I
K+pm5QEmqhrrfqP15qTs/CRC+vthaWz3IX9l1WgTo6y4nu2p6ESJdc+6W9Xe3Af2
EVu24m1gMgvbcay5vQpY5yH+9C1Bxs0V9155qE9EfWqKbWao/A+RGV/mbrguPwIX
BexBiu7GTjGUAsMpb129qUotwNfdLMawjFZ8TXq7HXewajspIiwpirbxWC9SqIRW
qKE6gxkxMcY+jhBatJr7nvrrHBRDx0EeL6MimDoeCDZZn1UNt03EqTin4gGPPtEa
fH1sLfcrDSOH3TSQb2X4K3jntstH/daV8KhKP2E2O3vvR2H58q9qpUxRGFspCduD
W0cL+/NBd168rag/nDugCGr0qUSrkqpNlL+fDDosfBHh8Qy0joDtnOiq4wdw0vMi
waUC2x6edw4GKAvC/elKtnK9WRH5NoWa5p9PY4H5i5AWXmii5IbMIHqr25nUvAL9
sS7NcdOuFvR05k9dce8kroj0UHxdU2LrcMGGpzS770hXjkZSrCGtzN2WZQARAQAB
tBtHaXRMYWIgQ1ZFIDxjdmVAZ2l0bGFiLmNvbT6JAlQEEwEKAD4CGwMFCwkIBwIG
FQoJCAsCBBYCAwECHgECF4AWIQSN7uA0VTv5j3YMqdL6be/LIZJiyAUCX3yoDwUJ
AbAXZwAKCRD6be/LIZJiyDUsD/4km9Gmc4cX9ucdOQe8X48MDLa4ogy/hZhUy+Nb
+zzq4eA7vQL9museB2as4RD41kyloonS9WtyLZ2kw+Wevu2GC1alhXsM7i9ZHFrl
theVbk8v+prNuOqcfpLtiQcxDCwClZKw7g7NVNO5lzZJ6G+Qok0VkGcXjK60Yw/i
vpQK/C9wSSGIbV/97M4717twZT/D+hmV04ZMAar30tI+271djihnxknzrjOyPcNY
5u2FzMGRVbEEPO6xAk2C1BMLzgJczoSfat4YBdURF9QnAFKPJRJF9Dbik/XmDh85
kK0GwYxOpmi7d6CvEIidSS/SAMVwgAkqdwYME8J+mKW3n/ZpANH2Eu2pEWDDUkL9
oYMjc7xFnM0QmhmRivzLjsgQCk3/bTtktoh8XX+MbP+/0HiBU+yyU/q76ZwsI8cJ
O1rnvgUjvZ0qwEroWQt/ovWgJgY+66jSYa3xQUd9fa6M8LAi4tnpSfW7qK725MQN
2Sir2Bijpco1zZ8OD+5ctdiic3TmjDmmHkG1dOBSPgFHEygyeVdmin8pRUNOV4dc
CFhX5WXgfdxxTz7SHTBPlxeRjCqjsJ6Tu3IaU7n7OBIGslmsUuSEq0Hbm7zfI+mx
HgLy2x2UYv9pAkHm7Jn/gQNH7kb2Rr+Z7LGSwFOO+1/zdQUioKZIEooUf1klIuOe
kyQjw7kCDQReud6oARAAx52wVpt9TzMbFOWzL+/yPjn01BlrssbQ/XZsDPUzgZGL
cRy55AtivBPhFl83KvgDXJaEmpwf5eA17jk3Az617Uz2MPTbXIVKddTS2d2DTsBd
dCsw7uAMjv6naJVSLVwFcPsgkpYSaWcegFjmOfBBFZnF+WL45yqFLk1E/Ek67ciB
/cQ32pPgce5+p/QZACyzWnlWrzPCyJJYKItFb160lXajpKLgT5X9b7dQ8M1G7oTw
rEaafxT3Mt+ICSlfWFZqd+cjLGswEfTtKF9jJJ/3GJw8MKgW3NFjEqt1U6HVNfZf
PfWOBe3pxlh9yS3d+1DGX/ornZKB3w1F7z4IUQAiMJZM+8VBQRYvrjLuyGlwhjTd
AiDzSDSvbNixP/BMdoZ//bZgej6FU711wqwMPqlIQS6WbyBrE6LmXjll/mahjgF0
jhDuBqgY8Sw3qLzcIWD1LGATeKVdNLpbVoUWwE9+J9XMNanVAtEZqD7AYxu24pVN
eoMLdrrfqt+HoLl8ziy3Ib3DYeNP/TkZH8talxJtq5wZ9E74PRtxk0XkVBjUEnFV
TjwYIkuOde1MJ8Xy1tzGSz0jFGP6ivom/DeK2Cm/hOroc8KW7s3HhSI+XJ7oclWU
buIzgkMtz5op9xofQ05lMw4kymghl9F5TvZm8AUd0H5GwWEv+SEF8JzeZ1m4WwMA
EQEAAYkCPAQYAQoAJhYhBI3u4DRVO/mPdgyp0vpt78shkmLIBQJeud6oAhsMBQkB
4TOAAAoJEPpt78shkmLI3wEQAJNHo9dBQNOX2/ehKWkwADdHFf48zwHvQCEK+KW6
Yl7xTRadIESzenRp+x5vCnwLe52FAjtHrW2q856/PQT3xQUHx26txsGIBFwCijrp
/jM32nM5scYI8yzu7GAteJuZC+FXWdHqzPjSJyf7fI4IEY3fNfnuusBUFKIiVupY
OP+PrZsZl8HwuL3Ojp9O/TWWzg7z/aA/JbL8JYz7RpLfk5pPCt+2yzXvoLCaIv+0
p6a8lm74i7fXtYdzK83wS4tT2AcPNGA4pGtOpSs8I5O0FxTGNiMXqV4qkt3VUgOn
qlJ1lXskM5BmyjajyWcyzn484kY+IQ4kjnli6ZNWHBGdVF9RCL8PIIttZsL2D6TS
mPhQSVdlWiMTexM1BxEtHbS5Q4G+hkhuU80bP4iwbz3sSO8+v+expI9CIRFrmZMI
2hwfpqKspK/z9meg4olnjUMOTU4OXu2TNwfUuYFJcwJ4IrDjAguKJXSa4fVTuh8W
e+JvhzpPqj0olU9TqeCSt79BZtm+nX352lOrl4YjwKY52oN9BNPDK/lf4u6HFkCw
s0YiVJ+Ch0vKVuWJaXKBx8zuHkZAc1/ADV9pIYZo1psJzJiONhQrwX+QsyWVDMFx
x07fbuhepzlhXvHT5GW5wTvRSnaDRUaYA2QTxLJTvUGSg9cOY1SGvdIvgVFVTPUO
C7kX
=3Djo
-----END PGP PUBLIC KEY BLOCK-----
```
